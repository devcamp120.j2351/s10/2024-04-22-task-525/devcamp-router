import { BrowserRouter, Routes, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import FullPage from "./page/page";

function App() {
    return (
        <div>
            <FullPage/>
        </div>
    );
}

export default App;
