import FirstPage from "./components/firstpage";
import HomePage from "./components/home";
import SecondPage from "./components/secondpage";
import ThirdPage from "./components/thirdpage";

const RoutesList = [
    {
        path: "/",
        element: <HomePage/>
    },
    {
        path: "/firstpage",
        element: <FirstPage/>
    },
    {
        path: "/secondpage",
        element: <SecondPage/>
    },
    {
        path: "/secondpage/:param",
        element: <SecondPage/>
    },
    {
        path: "*",
        element: <ThirdPage/>
    }
]

export default RoutesList;