import { useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";

const SecondPage = () => {
    const {param} = useParams();
    console.log (param);
    const navigate = useNavigate();
    useEffect(()=>{
        if (param == "third"){
            navigate("/thirdpage");
        }
    })
    return (
        <>
            <h1>Đây là trang Second page</h1>
            <p>Giá trị của param 1 là: {param}</p>
        </>    
    )
}

export default SecondPage;