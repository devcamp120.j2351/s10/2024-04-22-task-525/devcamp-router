import { Nav, NavItem, NavLink } from "reactstrap";
const HeaderPart = () => {
    return (

        <Nav>
            <NavItem>
                <NavLink href="/">Trang chủ</NavLink>
            </NavItem>
            <NavItem>
                <NavLink href="/firstpage">Trang Firstpage</NavLink>
            </NavItem>
            <NavItem>
                <NavLink href="/secondpage">Trang Secondpage</NavLink>
            </NavItem>
            <NavItem>
                <NavLink href="/thirdpage">Trang Thirdpage</NavLink>
            </NavItem>
        </Nav>
    )
}

export default HeaderPart;