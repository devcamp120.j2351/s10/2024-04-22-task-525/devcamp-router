import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Container, Row, Col } from "reactstrap";
import RoutesList from "../App.routes";
const MainPart = () => {
    return (
        <Container>
            <Row>
                <Col className="bg-light border">
                    <BrowserRouter>
                        <Routes>
                            {
                                RoutesList.map((elm, key) => {
                                    return (
                                        <Route path={elm.path} element={elm.element}></Route>
                                    )
                                })
                            }

                        </Routes>
                    </BrowserRouter>
                </Col>
            </Row>
        </Container>
    )
}

export default MainPart;