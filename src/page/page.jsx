import FooterPart from "./footer"
import HeaderPart from "./header"
import MainPart from "./maincontent"

const FullPage = () => {
    return (
        <>
            <HeaderPart />
            <MainPart />
            <FooterPart />
        </>
    )
}

export default FullPage;